import { CreateAveriasComponent } from './components/create-averias/create-averias.component';
import { ListAveriasComponent } from './components/list-averias/list-averias.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: 'list-averias', pathMatch: 'full'},
  { path: 'list-averias', component: ListAveriasComponent},
  { path: 'create-averias', component: CreateAveriasComponent},
  { path: 'editAveria/:id', component: CreateAveriasComponent},
  { path: '**', redirectTo: 'list-averias', pathMatch: 'full'},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

import { ToastrService } from 'ngx-toastr';
import { AveriaService } from './../../services/averia.service';
import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/compat/firestore';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-list-averias',
  templateUrl: './list-averias.component.html',
  styleUrls: ['./list-averias.component.css']
})
export class ListAveriasComponent implements OnInit {
  averias: any[] = [];

  constructor(private _averiaService: AveriaService,
              private toastr: ToastrService) {

  }

  ngOnInit(): void {
    this.getAveria()
  }

  getAveria (){
    this._averiaService.getAverias().subscribe(data => {
      this.averias = [];
      data.forEach((element:any) => {
       /* console.log(element.payload.doc.id);*/ /* Muestra la ID */
       /* console.log(element.payload.doc.data()); */ /*Muestra la Información */
       this.averias.push({
         id: element.payload.doc.id,
         ...element.payload.doc.data()
       })
      });
      console.log(this.averias)
    });
  }

  eliminarAveria (id: string) {
    this._averiaService.eliminarAveria(id).then(() =>{
      console.log('Avería eliminada con exito');
      this.toastr.error('La avería fue eliminada con exito', '¡Registro eliminado!', {
        positionClass: 'toast-bottom-right'
      });
    }) .catch(error => {
      console.log(error);
    })
  }
}

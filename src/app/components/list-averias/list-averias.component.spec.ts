import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListAveriasComponent } from './list-averias.component';

describe('ListAveriasComponent', () => {
  let component: ListAveriasComponent;
  let fixture: ComponentFixture<ListAveriasComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListAveriasComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ListAveriasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { AveriaService } from './../../services/averia.service';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-create-averias',
  templateUrl: './create-averias.component.html',
  styleUrls: ['./create-averias.component.css']
})
export class CreateAveriasComponent implements OnInit {
  createAveria: FormGroup;
  submitted = false;
  loading = false;
  id: string | null;
  titulo = 'Agregar Avería';

  constructor(private fb: FormBuilder,
              private _averiaService: AveriaService,
              private router: Router,
              private toastr: ToastrService,
              private aRoute: ActivatedRoute) {
    this.createAveria = this.fb.group({
      nombre: ['', Validators.required],
      apellido: ['', Validators.required],
      documento: ['', Validators.required],
      salario: ['', Validators.required],
    })
    this.id = this.aRoute.snapshot.paramMap.get('id');
    console.log(this.id)
   }


  ngOnInit(): void {
    this.esEditar();
  }
  agregarEditarAveria(){
    this.submitted = true;

    if(this.createAveria.invalid){
      return;
    }
    if(this.id === null){
      this.agregarAveria();
    }else{
      this.editarAveria(this.id);
    }
  }

  agregarAveria() {
    const averia: any = {
      nombre: this.createAveria.value.nombre,
      apellido: this.createAveria.value.apellido,
      documento: this.createAveria.value.documento,
      salario: this.createAveria.value.salario,
      fechaCreacion: new Date(),
      fechaActualizacion: new Date()
    }
    this.loading = true;
    this._averiaService.agregarAveria(averia).then(() => {
      this.toastr.success('¡La Avería fue registrada con exito!', 'Avería Registrada', {
        positionClass: 'toast-bottom-right'
      });
      this.loading = false;
      this.router.navigate(['/list-averias'])
    }).catch(error => {
      console.log(error);
      this.loading = false;
    })
  }

  editarAveria (id: string) {
    const averia: any = {
      nombre: this.createAveria.value.nombre,
      apellido: this.createAveria.value.apellido,
      documento: this.createAveria.value.documento,
      salario: this.createAveria.value.salario,
      fechaActualizacion: new Date()
    }

    this.loading = true;
    this._averiaService.actualizarAveria(id, averia).then(() => {
      this.loading = false;
      this.toastr.info ('La avería ha sido modificada con exito', 'Avería modificada', {
        positionClass: 'toast-bottom-right'
      })
      this.router.navigate(['/list-averias'])
    })
  }


  esEditar () {
    this.titulo = 'Editar Avería';
    if(this.id !== null) {
      this.loading = true;
      this._averiaService.getAveria(this.id).subscribe(data => {
        this.loading = false;
        console.log(data.payload.data()['nombre']);
        this.createAveria.setValue({
          nombre: data.payload.data()['nombre'],
          apellido: data.payload.data()['apellido'],
          documento: data.payload.data()['documento'],
          salario: data.payload.data()['salario'],
        })
      })
    }
  }
}

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateAveriasComponent } from './create-averias.component';

describe('CreateAveriasComponent', () => {
  let component: CreateAveriasComponent;
  let fixture: ComponentFixture<CreateAveriasComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CreateAveriasComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateAveriasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

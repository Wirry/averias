import { Observable } from 'rxjs';
import { AngularFirestore } from '@angular/fire/compat/firestore';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AveriaService {

  constructor(private firestore: AngularFirestore) { }

  agregarAveria(averia: any): Promise<any> {
    return this.firestore.collection('averias').add(averia);
  }

  getAverias(): Observable<any> {
    return this.firestore.collection('averias', ref => ref.orderBy('fechaCreacion', 'asc')).snapshotChanges();
  }

  eliminarAveria(id: string): Promise<any> {
    return this.firestore.collection('averias').doc(id).delete();
  }

  getAveria (id: string): Observable<any> {
    return this.firestore.collection('averias').doc(id).snapshotChanges();
  }

  actualizarAveria(id: string, data:any): Promise<any> {
    return this.firestore.collection('averias').doc(id).update(data);
  }
}

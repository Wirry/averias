// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    projectId: 'averias-2394e',
    appId: '1:598360690826:web:013666ccedf72061959d73',
    storageBucket: 'averias-2394e.appspot.com',
    apiKey: 'AIzaSyBhkJez_ctU3BJmDP7XXGmqFG87cUIB-8M',
    authDomain: 'averias-2394e.firebaseapp.com',
    messagingSenderId: '598360690826',
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
